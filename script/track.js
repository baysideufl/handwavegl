const video = document.getElementById("myvideo");
let trackButton = document.getElementById("trackbutton");
let updateNote = document.getElementById("updatenote");
const trackcanvas = document.getElementById("trackcanvas");
const context = trackcanvas.getContext("2d");

let isVideo = false;
let model = null;
let videoInterval = 50

const modelParams = {
  flipHorizontal: true, // flip e.g for video
  maxNumBoxes: 1, // maximum number of boxes to detect
  iouThreshold: 0.5, // ioU threshold for non-max suppression
  scoreThreshold: 0.6, // confidence threshold for predictions.
}

function startVideo() {
  handTrack.startVideo(video).then(function (status) {
      console.log("video started", status);
      if (status) {
          updateNote.innerText = "Now tracking"
          isVideo = true
          runDetection()
      } else {
          updateNote.innerText = "Please enable video"
      }
  });
}

function toggleVideo() {
  if (!isVideo) {
      updateNote.innerText = "Starting video"
      startVideo();
  } else {
      updateNote.innerText = "Stopping video"
      handTrack.stopVideo(video)
      isVideo = false;
      updateNote.innerText = "Video stopped"
  }
}

trackButton.addEventListener("click", function () {
  console.log('track');
  toggleVideo();
});

function runDetection() {
  model.detect(video).then(predictions => {
      // get the middle x value of the bounding box and map to paddle location
      model.renderPredictions(predictions, trackcanvas, context, video);
      if (predictions[0]) {
          let midvalX = predictions[0].bbox[0] + (predictions[0].bbox[2] / 2);
          let posX = document.body.clientWidth * (midvalX / video.width);

          let midvalY = predictions[0].bbox[1] + (predictions[0].bbox[3] / 2);
          let posY = document.body.clientHeight * (midvalY / video.height);
          handMove(posX, posY, predictions[0].bbox[2], predictions[0].bbox[3]);
      }
      if (isVideo) {
          setTimeout(() => {
              runDetection(video)
          }, videoInterval);
      }
  });
}

// Load the model.
handTrack.load(modelParams).then(lmodel => {
  // detect objects in the image.
  model = lmodel
  updateNote.innerText = "Loaded Model!"
  trackButton.disabled = false
});