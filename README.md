# WebGL Fluid Simulation

[Play here](https://paveldogreat.github.io/WebGL-Fluid-Simulation/)

![Screenshot](screenshot.jpg "Screenshot")

## Libraries

- [HandTrackJS](https://towardsdatascience.com/handtrackjs-677c29c1d585)
- [WebGL Fluid Simulation](https://github.com/PavelDoGreat/WebGL-Fluid-Simulation)

## Running the Project

Run a basic web server on port `5500` and visit:
```
http://127.0.0.1:5500/index.html
```

## References

http://developer.download.nvidia.com/books/HTML/gpugems/gpugems_ch38.html

https://github.com/mharrys/fluids-2d

https://github.com/haxiomic/GPU-Fluid-Experiments

## License

The code is available under the [MIT license](LICENSE)
